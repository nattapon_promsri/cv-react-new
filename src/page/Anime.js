import React, { useState, useEffect } from "react"
import { Input, List, Icon, Select } from 'antd';

const IconText = ({ type, text }) => (
    <span>
        <Icon type={type} style={{ marginRight: 8 }} />
        {text}
    </span>
);

const Anime = () => {

    const [animeData, setAnimeData] = useState([])
    const [animeName, setAnimeName] = useState('naruto&limit=27')
    const [isLoading, setIsLoading] = useState(false)
    const [animeType, setanimeType] = useState([])
    const [selectedType, setSelectedType] = useState(-1)
    const { Search } = Input;
    const { Option } = Select;

    useEffect(() => {
        fetchAnime();
    }, [animeName]);

    const fetchAnime = () => {
        fetch('https://api.jikan.moe/v3/search/anime?q=' + animeName)
            .then(Response => Response.json())
            .then(data => {
                //set data(anime)
                setIsLoading(false)
                setAnimeData(data.results)
                genAnimeType(data.results)

            })
            .catch(error => console.log(error));
    }


    const genAnimeType = (animes) => {
        let typeList = []

        animes.map(anime => {
            if (!typeList.includes(anime.type)) {
                typeList.push(anime.type)
            }
        })
        setanimeType(typeList)
    }

    const onSearch = value => {
        setIsLoading(true)
        setAnimeName(value)

    }

    const selectType = (value) => {
        let keepType = value
        setSelectedType(keepType)
    }


    return (
        <div className="container" style={{backgroundColor: "lightblue"}}>
            <center>
                <h1>Anime</h1>
            </center>

            <div>
                <Search
                    style={{ width: 500 }}
                    placeholder="Input Anime Name"
                    enterButton="Search"
                    loading={isLoading}
                    size="large"
                    onSearch={onSearch}
                />


                <Select defaultValue={-1} style={{ width: 500, float: 'right' }} onChange={selectType}>
                    <Option value={-1}>All Anime</Option>
                    {animeType.map((typeAnime) => {
                        return (
                            <Option value={typeAnime}>{typeAnime}</Option>
                        )
                    })}
                </Select>

            </div>

            <List
                itemLayout="vertical"
                size="large"
                loading={isLoading}
                pagination={{
                    onChange: page => {
                        console.log(page);
                    },
                    pageSize: 3,
                }}
                dataSource={
                    selectedType == -1 ?
                        animeData
                        :
                        animeData.filter(m => m.type == selectedType)

                }


                renderItem={item => (
                    <List.Item
                        key={item.title}
                        actions={[
                            <IconText type="star-o" text={item.type} key="list-vertical-star-o" />,
                            <IconText type="like-o" text={item.score} key="list-vertical-like-o" />,
                            <IconText type="message" text={item.episodes} key="episodes" />,
                        ]}
                        extra={
                            <img
                                width={250}
                                height={250}
                                alt="logo"
                                src={item.image_url}
                            />
                        }
                    >
                        <List.Item.Meta
                            title={<a href={item.href}>{item.title}</a>}
                            description={item.synopsis}
                        />
                        {item.content}
                    </List.Item>
                )}
            />


        </div>
    )
}
export default Anime;
