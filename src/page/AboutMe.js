import React from 'react'
import { List, Typography } from 'antd';


const data = [
    'My Name : Nattapon Promsri',
    ' My NickName : Nat',
    'My Birthday : 26/10/1998',
    'Hobby : Play Game , Listen to music'
]

class AboutMe extends React.Component {

    render() {
        return (
            <div className="container" style={{backgroundColor: "lightblue"}}>
                <h2>About Me</h2>
                <center>
                    <div className="row-12">

                        <div className="col-6">
                            <img src={require('../nattapon_promsri.jpg')} height="250 px" width="250 px"></img>
                        </div>
                        <div className="col-6">
                            <List
                                size="large"

                                bordered
                                dataSource={data}
                                renderItem={item => <List.Item>{item}</List.Item>}
                            />

                        </div>
                    </div>
                </center>
                <br></br><br></br><br></br><br></br>
                <div class="section lb">
                    <div class="container">
                        <div class="section-title text-center">
                            <h3>My Skill</h3>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="services-inner-box">
                                    <div class="ser-icon">
                                        <i class="flaticon-idea-1"></i>
                                    </div>
                                    <h2>Computer Language</h2>
                                    <h3>HTML 30 %</h3>
                                    <h3>PHP 10 %</h3>
                                    <h3>Java 20 %</h3>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="services-inner-box">
                                    <div class="ser-icon">
                                        <i class="flaticon-discuss-issue"></i>
                                    </div>
                                    <h2>Communication</h2>
                                    <h3>Thai 90 %</h3>
                                    <h3>English 40 %</h3>
                                    <h3>Japanese 5 %</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br></br><br></br><br></br><br></br>




                <script src="js/all.js"></script>

                <script src="js/jquery.mobile.customized.min.js"></script>
                <script src="js/jquery.easing.1.3.js"></script>
                <script src="js/parallaxie.js"></script>
                <script src="js/slick.min.js"></script>
                <script src="js/animated-slider.js"></script>

                <script src="js/jqBootstrapValidation.js"></script>
                <script src="js/contact_me.js"></script>

                <script src="js/custom.js"></script>

            </div>

        )
    }
}

export default AboutMe;