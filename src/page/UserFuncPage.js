import React, { useState } from 'react'

// class UserFuncPage extends React.Component{

//     render(){
//         return 
//     }
// }

const UserFuncPage = () => {
    const [user, serUser] = useState([]);

    fetchUser() {
        fetch('https://jsonplaceholder.typicode.com/users')
            .then(Response => Response.json())
            .then(data => {
                console.log(data);
                this.setState({
                    user: data
                })
            })
            .catch(error => console.log(error));
    }


    render() {
        var { user } = this.state;
        console.log(user);

        return (
            <div>
                <Header></Header>
                <div>
                    <h1>User Page</h1>
                </div>
                <div>
                    {
                        user.map((item, index) => {
                            return (
                                <div key={index}>
                                    Hello
                                <p>id: {item.id}</p>
                                    <p>Name: {item.name}</p>
                                    <p>UserName: {item.username}</p>
                                    <p>E-mail: {item.email}</p>
                                </div>
                            )
                        })}
                </div>
            </div>
        );
    }
}

export default UserFuncPage;