import React from 'react'

const allGrade = () => {
    table = [
        {
            enroll1: [
                {
                    id: '001201',
                    name: 'CRIT READ AND EFFEC WRITE',
                    credit: '3',
                    grade: 'F'
                },
                {
                    id: '201115',
                    name: 'LIFE AND ENERGY',
                    credit: '3',
                    grade: 'F'
                },
                {
                    id: '954245',
                    name: 'DATA MANAGEMENT',
                    credit: '3',
                    grade: 'F'
                },
                {
                    id: '954246',
                    name: 'ADV COMP PROGRAMMING FOR MM',
                    credit: '3',
                    grade: 'F'
                },
                {
                    id: '954260',
                    name: 'KNOWLEDGE MANAGEMENT SYSTEM',
                    credit: '3',
                    grade: 'F'
                },
                {
                    id: '954270',
                    name: 'ELEMENTARY BPM',
                    credit: '3',
                    grade: 'F'
                }
            ],
            gpa1: {
                enrollCredit: 18,
                receivedCredit: 18
            }
        },
        {
            enroll2: [
                {
                    id: '001229',
                    name: 'ENGLISH FOR MEDIA ARTS',
                    credit: '3',
                    grade: 'F'
                },
                {
                    id: '208263',
                    name: '	ELEMENTARY STATISTICS',
                    credit: '3',
                    grade: 'F'
                },
                {
                    id: '954240',
                    name: 'WEB PROGRAMMING',
                    credit: '3',
                    grade: 'F'
                },
                {
                    id: '954244',
                    name: 'STRUCTURAL ANALYSIS AND DESIGN',
                    credit: '3',
                    grade: 'F'
                },
                {
                    id: '954310',
                    name: '	INFO SYS FOR ERP',
                    credit: '3',
                    grade: 'F'
                },
                {
                    id: '954340',
                    name: '	ENTERPRISE DATABASE SYSTEM',
                    credit: '3',
                    grade: 'F'
                }
            ]
            ,
            gpa2: {
                enrollCredit: 18,
                receivedCredit: 36
            }
        },
        {
            enroll3: [
                {
                    id: '208263',
                    name: '	ELEMENTARY STATISTICS',
                    credit: '3',
                    grade: 'F'
                },
                {
                    id: '954370',
                    name: 'ANALYSIS DESIGN MATS MGMT',
                    credit: '3',
                    grade: 'F'
                },
                {
                    id: '954374',
                    name: 'ESD FOR DIGITAL MARKET',
                    credit: '3',
                    grade: 'F'
                },
                {
                    id: '954416',
                    name: 'INFO SYS FOR SCM AND CRM',
                    credit: '3',
                    grade: 'F'
                },
                {
                    id: '954426',
                    name: 'INTRODUCTION TO E-SERVICE',
                    credit: '3',
                    grade: 'F'
                },
                {
                    id: '954449',
                    name: 'RAPID APPLICATION DEVELOPMENT',
                    credit: '3',
                    grade: 'F'
                }
                ,
                {
                    id: '954477',
                    name: 'IT FOR PRODUCTION SYS',
                    credit: '3',
                    grade: 'F'
                }

            ]
            ,
            gpa3: {
                enrollCredit: 21,
                receivedCredit: 57
            }
        }
    ]
}

export default allGrade;



