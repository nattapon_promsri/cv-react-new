import React, { useState, useEffect } from 'react'
import { allGrade } from '../data';



const Gpa = () => {
    const [semesters, setSemesters] = useState([]);
    const [saveIndex, setSaveIndex] = useState(-1);
    // console.log(data)
    // saveIndex : -1

    useEffect(() => {
        fetchGrade();

    }, [])

    const fetchGrade = () => {
        setSemesters(allGrade);
    }



    const onChangeSelectBox = (e) => {
        let i = e.target.value
        setSaveIndex(i)
    }


    // alert(saveIndex)
    // const sem = [semesters[0]];

    return (
        <div>
            <div><h1>GPA</h1></div>
            
            <div>
                <select onChange={onChangeSelectBox}>
                    <option value={-1}>All</option>
                    {semesters.map((sem, index) => {

                        return (
                            <option value={index}>{sem.year}</option>
                        )
                    })}

                </select>
            </div>

            {
                semesters.map(({ year, enroll, gpa }, index) => {
                    // const { enroll, gpa } = semester

                    if (index == saveIndex || saveIndex == -1) {
                        return (

                            <div>
                                <h2>Semester :{index + 1}</h2>
                                <h2>year :{year}</h2>

                                <table className="table table-dark">
                                    <thead >
                                        <tr>
                                            <th >Course ID</th>
                                            <th>Course Name</th>
                                            <th>Credits</th>
                                            <th>Grade</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {enroll.map((subject, index) => {
                                            return (
                                                <tr>
                                                    <td>{subject.id}</td>
                                                    <td>{subject.name}</td>
                                                    <td>{subject.credit}</td>
                                                    <td>{subject.grade}</td>
                                                </tr>)
                                        })}

                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th colspan="2" class="text-center">Total</th>
                                            <th>{gpa.enrollCredit}</th>
                                            <th>{gpa.receivedCredit}</th>
                                        </tr>
                                    </tfoot>
                                </table>

                                }
                        </div>
                        )
                    }
                })
            }

            <div> <button className="btn btn-primary" onClick={() => { window.location = "/processLogout" }}>
                           Log Out
                        </button></div>
        </div>
    )

}

export default Gpa;



{/* <div> */ }
{/* {
                    data.map((item, index) => {
                        <div>

                            {console.log(item.enroll1)} */}

{/* {item.map((item2) => {
                                <div>
                                    <p>courseId :{item.id}</p>
                                    <p>Subject :{item.name}</p>
                                    <p>Credits :{item.credit}</p>
                                    <p>Grade :{item.grade}</p>

                                </div> */}
{/* })} */ }
{/* </div> */ }
{/* })
        }

            </div> */}
    //     const [courseId, subject, credits, grade] = useState([]);

    //     useEffect(() => {

    //     }, [])

    //     fetchUser() {
    //         fetch('./Grade.js')
    //             .then(Response => Response.json())
    //             .then(data => {
    //                 console.log(data);
    //                 this.setState({
    //                     user: data
    //                 })
    //             })
    //             .catch(error => console.log(error));
    //     }


    //     render() {

    //         var { user } = this.state;
    //         console.log(user);

    //         return (
    //             <div>
    //                 <Header></Header>

    //                 <div>
    //                     <button className="btn btn-primary" onClick={() => { window.location = "/processLogout" }}>
    //                         Log Out
    //                     </button>
    //                 </div>
    //             </div>

    //         )
    //     }


// <center>
//                     <h2>GPA</h2>
//                     <div>
//                         <h3>Semester 1/2561</h3>


//                         <table className="table table-dark">
//                             <thead >
//                                 <tr>
//                                     <th >Course ID</th>
//                                     <th>Course Name</th>
//                                     <th>Credits</th>
//                                     <th>Grade</th>
//                                 </tr>
//                             </thead>
//                             <tbody>
                                // <tr>
                                //     <td>001201</td>
                                //     <td>CRIT READ AND EFFEC WRITE</td>
                                //     <td>3</td>
                                //     <td>F</td>
                                // </tr>
//                                 <tr>
//                                     <td>201115</td>
//                                     <td>LIFE AND ENERGY</td>
//                                     <td>3</td>
//                                     <td>F</td>
//                                 </tr>
//                                 <tr>
//                                     <td>954245</td>
//                                     <td>DATA MANAGEMENT</td>
//                                     <td>3</td>
//                                     <td>F</td>
//                                 </tr>
//                                 <tr>
//                                     <td>954246</td>
//                                     <td>ADV COMP PROGRAMMING FOR MM</td>
//                                     <td>F</td>
//                                     <td>F</td>
//                                 </tr>
//                                 <tr>
//                                     <td>954260</td>
//                                     <td>KNOWLEDGE MANAGEMENT SYSTEM</td>
//                                     <td>3</td>
//                                     <td>F</td>
//                                 </tr>
//                                 <tr>
//                                     <td>954270</td>
//                                     <td>ELEMENTARY BPM</td>
//                                     <td>3</td>
//                                     <td>F</td>
//                                 </tr>
//                             </tbody>
//                             <tfoot>
//                                 <tr>
//                                     <th colspan="2" class="text-center">Total</th>
//                                     <th>18</th>
//                                     <th>0.00</th>
//                                 </tr>
//                             </tfoot>
//                         </table>
//                     </div>


//                     <div >
//                         <h3>Semester 2/2561</h3>
//                     </div>
//                     <div>
//                         <table className="table table-dark">
//                             <thead >
//                                 <tr>
//                                     <th>Course ID</th>
//                                     <th>Course Name</th>
//                                     <th>Credits</th>
//                                     <th>Grade</th>
//                                 </tr>
//                             </thead>
//                             <tbody>
//                                 <tr>
//                                     <td>001229</td>
//                                     <td>ENGLISH FOR MEDIA ARTS</td>
//                                     <td>3</td>
//                                     <td>D</td>
//                                 </tr>
//                                 <tr>
//                                     <td>208263</td>
//                                     <td>ELEMENTARY STATISTICS</td>
//                                     <td>3</td>
//                                     <td>D</td>
//                                 </tr>
//                                 <tr>
//                                     <td>954240</td>
//                                     <td>WEB PROGRAMMING</td>
//                                     <td>3</td>
//                                     <td>D</td>
//                                 </tr>
//                                 <tr>
//                                     <td>954244</td>
//                                     <td>STRUCTURAL ANALYSIS AND DESIGN</td>
//                                     <td>3</td>
//                                     <td>D</td>
//                                 </tr>
//                                 <tr>
//                                     <td>954310</td>
//                                     <td>INFO SYS FOR ERP</td>
//                                     <td>3</td>
//                                     <td>D</td>
//                                 </tr>
//                                 <tr>
//                                     <td>954340</td>
//                                     <td>ENTERPRISE DATABASE SYSTEM</td>
//                                     <td>3</td>
//                                     <td>D</td>
//                                 </tr>
//                             </tbody>
//                             <tfoot>
//                                 <tr>
//                                     <th colspan="2" className="text-center">Total</th>
//                                     <th>18</th>
//                                     <th>Sercret</th>
//                                 </tr>
//                             </tfoot>
//                         </table>
//                     </div>

//                     <div>
//                         <h3>Semester 1/2562</h3>
//                     </div>
//                     <div>
//                         <table className="table table-dark">
//                             <thead >
//                                 <tr>
//                                     <th>Course ID</th>
//                                     <th>Course Name</th>
//                                     <th>Credits</th>
//                                     <th>Grade</th>
//                                 </tr>
//                             </thead>
//                             <tbody>
//                                 <tr>
//                                     <td>208263</td>
//                                     <td>ELEMENTARY STATISTICS</td>
//                                     <td>3</td>
//                                     <td>C+</td>
//                                 </tr>
//                                 <tr>
//                                     <td>954370</td>
//                                     <td>ANALYSIS DESIGN MATS MGMT</td>
//                                     <td>3</td>
//                                     <td>B+</td>
//                                 </tr>
//                                 <tr>
//                                     <td>954374</td>
//                                     <td>ESD FOR DIGITAL MARKET</td>
//                                     <td>3</td>
//                                     <td>B</td>
//                                 </tr>
//                                 <tr>
//                                     <td>954416</td>
//                                     <td>INFO SYS FOR SCM AND CRM</td>
//                                     <td>3</td>
//                                     <td>B</td>
//                                 </tr>
//                                 <tr>
//                                     <td>954426</td>
//                                     <td>	INTRODUCTION TO E-SERVICE</td>
//                                     <td>3</td>
//                                     <td>B</td>
//                                 </tr>
//                                 <tr>
//                                     <td>954449</td>
//                                     <td>	RAPID APPLICATION DEVELOPMENT</td>
//                                     <td>3</td>
//                                     <td>C</td>
//                                 </tr>
//                                 <tr>
//                                     <td>954477</td>
//                                     <td>IT FOR PRODUCTION SYS</td>
//                                     <td>3</td>
//                                     <td>B</td>
//                                 </tr>
//                             </tbody>
//                             <tfoot>
//                                 <tr>
//                                     <th colspan="2" className="text-center">Total</th>
//                                     <th>21</th>
//                                     <th>2.97</th>
//                                 </tr>
//                             </tfoot>
//                         </table>
//                     </div>

