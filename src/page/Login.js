import React from 'react'

class Login extends React.Component {

    render() {
        return (
            <div className="container" style={{backgroundColor: "lightblue"}}>
                <center>
                    <h1>Login Page</h1>
                    <div>
                        <h3>Username : <input type="text" placeholder="username"></input></h3>
                    </div>
                    <div>
                        <h3>Password : <input type="password" placeholder="password"></input></h3>
                    </div>
                    <div>
                        <button className="btn btn-primary" onClick={() => { window.location = "/processLogin" }}>
                            Login
                    </button>
                    </div>
                </center>
            </div>

        )
    }

}

export default Login;