import React from 'react'

class Header extends React.Component {

    render() {
        return (
            <div>
                <center>
                    <div >

                        {/* <div className="btn-group" role="group" aria-label="Basic example">
                        <button type="button" className="btn btn-secondary"><a href="/home">Home</a></button>
                        <button type="button" className="btn btn-secondary"><a href="/aboutme">About Me </a></button>
                        <button type="button" className="btn btn-secondary"><a href="/skill">Skill</a></button>
                        <button type="button" className="btn btn-secondary"><a href="/gpa">GPA</a></button>
                    </div> */}

                        <ol class="breadcrumb">
                            {/* <li> <a href="/home">Home || </a></li> */}
                            <li> <a href="/aboutme">About Me |</a></li>
                            {/* <li> <a href="/skill">| Skill |</a></li> */}
                            <li> <a href="/gpa">| GPA |</a></li>
                            <li> <a href="/anime">| Anime</a></li>
                        </ol>

                    </div>
                </center>
            </div >

        )
    }

}

export default Header;