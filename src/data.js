export const allGrade = [
    {
        year: 2560,
        enroll: [
            {
                id: '001201',
                name: 'CRIT READ AND EFFEC WRITE',
                credit: '3',
                grade: 'F'
            },
            {
                id: '201115',
                name: 'LIFE AND ENERGY',
                credit: '3',
                grade: 'F'
            },
            {
                id: '954245',
                name: 'DATA MANAGEMENT',
                credit: '3',
                grade: 'F'
            },
            {
                id: '954246',
                name: 'ADV COMP PROGRAMMING FOR MM',
                credit: '3',
                grade: 'F'
            },
            {
                id: '954260',
                name: 'KNOWLEDGE MANAGEMENT SYSTEM',
                credit: '3',
                grade: 'F'
            },
            {
                id: '954270',
                name: 'ELEMENTARY BPM',
                credit: '3',
                grade: 'F'
            }
        ],
        gpa: {
            enrollCredit: 18,
            receivedCredit: 18
        }
    },
    {
        year: 2561,
        enroll: [
            {
                id: '001229',
                name: 'ENGLISH FOR MEDIA ARTS',
                credit: '3',
                grade: 'F'
            },
            {
                id: '208263',
                name: '	ELEMENTARY STATISTICS',
                credit: '3',
                grade: 'F'
            },
            {
                id: '954240',
                name: 'WEB PROGRAMMING',
                credit: '3',
                grade: 'F'
            },
            {
                id: '954244',
                name: 'STRUCTURAL ANALYSIS AND DESIGN',
                credit: '3',
                grade: 'F'
            },
            {
                id: '954310',
                name: '	INFO SYS FOR ERP',
                credit: '3',
                grade: 'F'
            },
            {
                id: '954340',
                name: '	ENTERPRISE DATABASE SYSTEM',
                credit: '3',
                grade: 'F'
            }
        ]
        ,
        gpa: {
            enrollCredit: 18,
            receivedCredit: 36
        }
    },
    {
        year: 2562,
        enroll: [
            {
                id: '208263',
                name: '	ELEMENTARY STATISTICS',
                credit: '3',
                grade: 'F'
            },
            {
                id: '954370',
                name: 'ANALYSIS DESIGN MATS MGMT',
                credit: '3',
                grade: 'F'
            },
            {
                id: '954374',
                name: 'ESD FOR DIGITAL MARKET',
                credit: '3',
                grade: 'F'
            },
            {
                id: '954416',
                name: 'INFO SYS FOR SCM AND CRM',
                credit: '3',
                grade: 'F'
            },
            {
                id: '954426',
                name: 'INTRODUCTION TO E-SERVICE',
                credit: '3',
                grade: 'F'
            },
            {
                id: '954449',
                name: 'RAPID APPLICATION DEVELOPMENT',
                credit: '3',
                grade: 'F'
            }
            ,
            {
                id: '954477',
                name: 'IT FOR PRODUCTION SYS',
                credit: '3',
                grade: 'F'
            }

        ]
        ,
        gpa: {
            enrollCredit: 21,
            receivedCredit: 57
        }
    }
]



