import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import Header from './page/Header';
import Home from './page/Home';
import Skill from './page/Skill';
import AboutMe from './page/AboutMe';
import { BrowserRouter, Route, Redirect } from 'react-router-dom';
import Gpa from './page/Gpa';
import Login from './page/Login';
import Anime from './page/Anime';
import 'antd/dist/antd.css';

import 'bootstrap/dist/css/bootstrap.min.css';


// //How to use local storage

// //setter
// localStorage.setItem("a", "Hello world");

// //getter
// const a = localStorage.getItem("a");
// alert(a);

const PrivateRoute = ({ path, component: Component }) => (
    <Route path={path} render={(props) => {
        const isLogin = localStorage.getItem("isLogin")

        //alert(inLogin)
        if (isLogin == 'true')
            return <Component {...props} />
        else
            return <Redirect to="/login" />

    }} />
);


const MainRouting =

    <BrowserRouter>
        <Route path="/processLogin" render={() => {
            localStorage.setItem("isLogin", true);
            return <Redirect to="/gpa" />
        }} />

        <Route path="/processLogout" render={() => {
            localStorage.setItem("isLogin", false);
            return <Redirect to="/aboutme" />
        }} />
        <Route path="/" component={Header} />
        <Route path="/" component={AboutMe} exact/>
        <Route path="/aboutme" component={AboutMe} />
        {/* <Route path="/skill" component={Skill} /> */}
        <PrivateRoute path="/gpa" component={Gpa} />
        <Route path="/login" component={Login} />
        <Route path="/anime" component={Anime} />
    </BrowserRouter>


ReactDOM.render(MainRouting, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
